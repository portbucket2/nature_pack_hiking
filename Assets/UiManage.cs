﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public static UiManage instance;
    public GameObject LevelEndPanel;
    public GameObject DecisionTurnPanel;
    public GameObject CapturePanel;
    public Text collectedItemsText;
    public Image whiteImage;
    public Color whiteColor;
    public Color whiteTransColor;
    public float GotItemsUi;
    public int maxItemsUi
    {
        get
        {
            return GameManageMent.instance.maxItems;
        }
    }
    public GameObject[] stars;

    public GameObject collectedText;

    public bool Collected;

    public bool atDecisionTurn;

    public pathPointBehavior DecisionPointScript;

    public float colfac;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        collectedText.SetActive(false);
        //LevelEndPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        ItemCountVisual();
        CollecetdTextVisual();
    }

    public void LevelEndPanelAppear()
    {
        LevelEndPanel.SetActive(true);

    }

    public void CapturePanelAppear()
    {
        CapturePanel.SetActive(true);
        LevelEndPanel.SetActive(false);
    }

    public void CloseAllPanels()
    {
        CapturePanel.SetActive(false);
        LevelEndPanel.SetActive(false);
    }

    public void ManagePanelAppearances()
    {
        //if(pathFollowing.instance.LevelEnd)
        //{
        //    LevelEndPanelAppear();
        //}
        //else
        //{
        //    LevelEndPanel.SetActive(false);
        //}
    }

    public void ItemCountVisual()
    {
        if (GameManageMent.instance.LevelEnd && GotItemsUi <= GameManageMent.instance.itemsCollected)
        {
            GotItemsUi += Time.deltaTime * 5;
            collectedItemsText.text =(int) GotItemsUi + " / "+ maxItemsUi;
            float starRatio = GotItemsUi / maxItemsUi;
            if(starRatio > 0.6f)
            {
                stars[0].SetActive(true);
                if (starRatio > 0.8f)
                {
                    
                    stars[1].SetActive(true);
                    if (starRatio > 0.99f)
                    {

                        stars[2].SetActive(true);
                    }
                }
            }

            if (colfac <= 1)
            {
                colfac += Time.deltaTime;
            }

        }
        else
        {
            
        }
        
        whiteImage.color = Color.Lerp(whiteColor, whiteTransColor, colfac);
    }

    public void RestartGame()
    {
        Application.LoadLevel(0);
    }

    public void CollecetdTextVisual()
    {
        if (Collected)
        {
            collectedText.SetActive(true);
            Collected = false;
            Invoke("DisappearCollectedText", 1f);
        }
    }

    void DisappearCollectedText()
    {
        collectedText.SetActive(false);
    }

    public void AppearDecisionTurn()
    {
        DecisionTurnPanel.SetActive(true);
    }
    public void DisAppearDecisionTurn()
    {
        DecisionTurnPanel.SetActive(false);
    }


    public void GoRightCopy()
    {
        DecisionPointScript.GoRight();
    }

    public void GoLeftCopy()
    {
        DecisionPointScript.GoLeft();
    }

}

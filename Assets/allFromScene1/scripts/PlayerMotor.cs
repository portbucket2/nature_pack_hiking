﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    public static PlayerMotor instance;
    public float walkSpeed;
    float walkSpeedApp;
    public float maxRotSpeed;
    float rotSpeedApp;
    public Vector2 trackPos;
    public GameObject camHolder;
    public GameObject camHolderSide;

    public Vector2 camRotApp;
    //public float maxcamRot;
    public float clickInY;
    public float clickInX;
    public Vector2 camRotProgress;
    public Vector2 camRotOld;
    public Vector2 camRotNew;

    public float snapSpeed;

    public bool rotDir;

    public Vector3 diff;

    public bool gotDear
    {
        get
        {
            return GameManageMent.instance.GotDeer;
        }
    }
    public Transform deerTarget;
    

    

    
    public float ClampAngle;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        AllignSideCamera();
    }

    // Update is called once per frame
    void Update()
    {
        ManageScreenTapPoint();
        ManageCamHolderRot();

        ManageWalk();
        //if (!gotDear)
        //{
        //   
        //    ManageWalk();
        //}
        //else
        //{
        //    pathFollowing.instance.paused = true;
        //}
        //else
        //{
        //    //camHolderSide.transform
        //}
        
        

        FollowPathFollower();





    }

    void ManageScreenTapPoint()
    {
        trackPos.x = (Input.mousePosition.x / Screen.width) - 0.5f;
        trackPos.y = (Input.mousePosition.y / Screen.height);
    }

    void ManageWalk()
    {
        if (Input.GetMouseButton(0) )
        {
            if (!gotDear)
            {
                pathFollowing.instance.paused = false;
            }
            else
            {
                pathFollowing.instance.paused = true;
            }
            
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, walkSpeed, Time.deltaTime * snapSpeed);
            if(Mathf.Abs(trackPos.x) > 0.2f)
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, trackPos.x * maxRotSpeed, Time.deltaTime * snapSpeed);
            }
            else
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            }

            camRotProgress.y = clickInY - trackPos.y;
            camRotProgress.x = clickInX - trackPos.x;

            camRotNew.y = camRotOld.y + camRotProgress.y * 150f;

            
            camRotNew.x = camRotOld.x + camRotProgress.x * 100f;
            camRotNew.y = Mathf.Clamp(camRotNew.y, -40f, 40f);

            //camRotNew.x = Mathf.Clamp(camRotNew.x, -60f, 60f);
            camRotNew.x = Mathf.Clamp(camRotNew.x, ClampAngle -70, ClampAngle + 70);



            camRotApp.y = Mathf.Lerp(camRotApp.y, camRotNew.y, Time.deltaTime *snapSpeed);
            camRotApp.x = Mathf.Lerp(camRotApp.x, camRotNew.x, Time.deltaTime * snapSpeed);
        }
        else
        {
            pathFollowing.instance.paused = true;
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, 0, Time.deltaTime * snapSpeed);
            rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            camRotProgress.y = 0;
            camRotProgress.x = 0;

            camRotApp.y = Mathf.Lerp(camRotApp.y, camRotNew.y, Time.deltaTime * snapSpeed);
            camRotApp.x = Mathf.Lerp(camRotApp.x, camRotNew.x, Time.deltaTime * snapSpeed);

            camRotNew.y = Mathf.Clamp(camRotNew.y, -40f, 40f);
            //camRotNew.x = Mathf.Clamp(camRotNew.x, -60f, 60f);
            camRotNew.x = Mathf.Clamp(camRotNew.x, ClampAngle - 70, ClampAngle + 70);


        }

        //transform.Translate(0, 0, walkSpeedApp*(1 - Mathf.Abs(trackPos.x * 2)) * Time.deltaTime * 50f);
        //transform.Translate(0, 0, walkSpeedApp  * Time.deltaTime * 50f);
        if (rotDir)
        {
            transform.Rotate(0, rotSpeedApp * Time.deltaTime * 50f, 0);
        }
        
    }

    void ManageCamHolderRot()
    {
        camHolder.transform.localRotation = Quaternion.Euler(camRotApp.y, 0 ,0);

        //camHolderSide.transform.position = this.transform.position;
        Vector3 locRot = Mathf.Rad2Deg * Quaternion.ToEulerAngles(camHolderSide.transform.localRotation);
        //Debug.Log(locRot);

        if (!gotDear)
        {
            Quaternion sideRot = Quaternion.Euler(0, -camRotApp.x, 0);
            camHolderSide.transform.rotation = Quaternion.Lerp(camHolderSide.transform.rotation, sideRot, Time.deltaTime * 50);
        }
        else
        {
            Quaternion sideRot = Quaternion.LookRotation((-camHolderSide.transform.position + deerTarget.position), transform.up);
            camHolderSide.transform.rotation = Quaternion.Lerp(camHolderSide.transform.rotation, sideRot, Time.deltaTime * 5);
        }
        
        

        
        diff.y = Vector3.SignedAngle(transform.forward, camHolder.transform.forward, transform.up);

        diff.x = Vector3.SignedAngle(transform.forward, camHolderSide.transform.forward, transform.up);

        ClampAngle = camRotApp.x + diff.x;

       



        if (Input.GetMouseButtonDown(0))
        {
            //camRotNew.x = -diffStored.y;
            camRotOld = camRotNew;
            clickInY = trackPos.y;
            clickInX = trackPos.x;
        }
        if (Input.GetMouseButtonUp(0))
        {
            
        }
    }

    void FollowPathFollower()
    {
        transform.position = Vector3.Lerp(transform.position, pathFollowing.instance.transform.position , Time.deltaTime * 5);
        transform.rotation = Quaternion.Lerp(transform.rotation, pathFollowing.instance.transform.rotation, Time.deltaTime * 2);
    }

    void AllignSideCamera()
    {
        camHolder.transform.localRotation = Quaternion.Euler(camRotApp.y, 0, 0);
        camHolderSide.transform.rotation = transform.rotation;

        Vector3 sideRot = Mathf.Rad2Deg * Quaternion.ToEulerAngles(camHolderSide.transform.rotation);
        Debug.Log(sideRot);

        camRotApp.x = camRotNew.x = camRotOld.x = -sideRot.y;

        diff.y = Vector3.SignedAngle(transform.forward, camHolder.transform.forward, transform.up);

        diff.x = Vector3.SignedAngle(transform.forward, camHolderSide.transform.forward, transform.up);

        ClampAngle = camRotApp.x + diff.x;
    }

    //public void GetDeer(Transform t)
    //{
    //    deerTarget = t;
    //    //gotDear = true;
    //}
    //
    //public void LeaveDeer()
    //{
    //    //gotDear = false;
    //}



    
}

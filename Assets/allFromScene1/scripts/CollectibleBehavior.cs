﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectibleBehavior : MonoBehaviour
{
    public GameObject FillCanvas;
    public Image radialBar;
    public float fillAmount;
    public bool fill;
    public int Index;
    public Text textIndex;

    public GameObject visual;

    public ItemsHubManager parentHub;
    //public GameObject holder;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FillCanvas.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position, Vector3.up);
        radialBar.fillAmount = fillAmount;
        if (fill)
        {
            FillIt();
        }
        else
        {
            fillAmount = 0;
        }
        fill = false;

        textIndex.text = "" + Index;
        
    }

    void FillIt()
    {
        if(fillAmount<= 1)
        {
            fillAmount += Time.deltaTime;
        }
        else
        {
            DestroyOwn();
        }
    }

    void DestroyOwn()
    {
        GameManageMent.instance.itemsCollected += 1;
        UiManage.instance.Collected = true;
        DisableOwn();
        //this.transform.gameObject.SetActive(false);
        if ( ItemHubCentral.instance.itemIndexLatest <= ItemHubCentral.instance.maxItemIndex)
        {
            ItemHubCentral.instance.IncreaseLatestIndex();
        }
            
        ItemHubCentral.instance.UpdateCollectedItems(Index);
        ItemHubCentral.instance. UpdateAllItemDisplay(parentHub);


    }

    public void EnableOwn()
    {
        visual.SetActive(true);
    }
    public void DisableOwn()
    {
        visual.SetActive(false);
    }


}

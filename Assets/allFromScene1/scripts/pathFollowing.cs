﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFollowing : MonoBehaviour
{
    
    public GameObject target;
    public GameObject PreviousPoint;
    public float AdvanceSpeed;
    public float RotSpeed;
    public float zFac;
    public int frames;
    public float initialYAngle;
    public float targetPathLength;
    public float tarYRot;

    public float cosA;

    public float finalYRot;
    float journey;
    public int stepsNeeded;
    Quaternion targetRot;

    public float steer;

    public Vector3 steerVector;
    public float directDistance;

    bool GotInitials;
    public bool paused;
    public bool LevelEnd;
    public bool AtDecisionPoint;
    public static pathFollowing instance;

    public bool easyApproach;
    public float rotSpeedMulti;

    public float Angle0;
    public float Angle1;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (!easyApproach)
        {
            CalculateInitials();
        }
        rotSpeedMulti = 0;


    }

    // Update is called once per frame
    void Update()
    {
        //CalculateInitials();
        if (!paused && !AtDecisionPoint)
        {
            MoveAhead();
        }

        
        //CalculateInitials();
    }


    void MoveAhead()
    {
        if (target)
        {
            this.transform.Translate(0, 0, AdvanceSpeed*Time.deltaTime * 50f);
            if(rotSpeedMulti < 1)
            {
                rotSpeedMulti += Time.deltaTime;
            }
            else
            {
                rotSpeedMulti = 1;
            }
            
            if (easyApproach)
            {
                steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
            }   steer = steerVector.x * RotSpeed * (AdvanceSpeed / 0.1f) * rotSpeedMulti;
            //steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
            //steer = steerVector.x * RotSpeed * (AdvanceSpeed / 0.1f);



            transform.Rotate(0, steer * Time.deltaTime * 50f, 0);
        }
        else
        {
            this.transform.Translate(0, 0, AdvanceSpeed * Time.deltaTime * 50f);
        }
        
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pathPoint")
        {
            //PreviousPoint = target;
            //target = null;

            if (!other.gameObject.GetComponent<pathPointBehavior>().DecisionTurn)
            {
                //PreviousPoint = target;
                target = null;
                if (PreviousPoint)
                {
                    if (other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject == PreviousPoint)
                    {
                        target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject;
                    }
                    else if (other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject == PreviousPoint)
                    {
                        target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
                    }
                    else
                    {
                        
                        Vector3 dir0 = (other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].transform.position);
                        Vector3 dir1 = (other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].transform.position);

                        Angle0 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir0);
                        Angle1 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward, dir1);

                        if (Angle0 < Angle1)
                        {
                            target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject;
                        }
                        else
                        {
                            target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
                        }

                    }
                }
                
                else
                {
                    
                    Vector3 dir0 = ( other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].transform.position);
                    Vector3 dir1 = (other.transform.position - other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].transform.position);
                    
                    Angle0 = Mathf.Rad2Deg* Vector3.AngleBetween(transform.forward, dir0);
                    Angle1 = Mathf.Rad2Deg * Vector3.AngleBetween(transform.forward , dir1);

                    if (Angle0 < Angle1)
                    {
                        target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[1].gameObject;
                    }
                    else
                    {
                        target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
                    }
                    
                }

                if (target.GetComponent<pathPointBehavior>().DecisionTurn && !PreviousPoint.GetComponent<pathPointBehavior>().DecisionTurn)
                {
                    UiManage.instance.DecisionPointScript = target.GetComponent<pathPointBehavior>();
                    UiManage.instance.AppearDecisionTurn();
                    AtDecisionPoint = true;

                    Debug.Log("1111");
                }

                PreviousPoint = other.transform.gameObject;
                //target = null;
            }
            else
            {
                if (PreviousPoint.GetComponent<pathPointBehavior>().DecisionTurn)
                {
                    //PreviousPoint = target;
                    target = null;
                    target = other.gameObject.GetComponent<pathPointBehavior>().TurnEntryPoint.gameObject;
                
                    Debug.Log("33333");
                }
                else
                {
                    //PreviousPoint = target;
                    target = null;
                    target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;

                    Debug.Log("2222");
                }

                PreviousPoint = other.transform.gameObject;

            }

            

            //target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
            //if (target.GetComponent<pathPointBehavior>().DecisionTurn && !other.gameObject.GetComponent<pathPointBehavior>().DecisionTurn)
            //{
            //
            //    
            //    
            //    //target = other.gameObject.GetComponent<pathPointBehavior>().TurnEntryPoint.gameObject;
            //}
            //if (target.GetComponent<pathPointBehavior>().DecisionTurn && other.gameObject.GetComponent<pathPointBehavior>().DecisionTurn)
            //{
            //    target = other.gameObject.GetComponent<pathPointBehavior>().nextPoints[0].gameObject;
            //
            //    Debug.Log("2222");
            //}
            //else if (PreviousPoint.GetComponent<pathPointBehavior>().DecisionTurn && other.gameObject.GetComponent<pathPointBehavior>().DecisionTurn)
            //{
            //    target = other.gameObject.GetComponent<pathPointBehavior>().TurnEntryPoint.gameObject;
            //
            //    Debug.Log("33333");
            //}
            
            if (!easyApproach)
            {
                CalculateInitials();
                
            }

            rotSpeedMulti = 0;



        }
        if(other.tag == "LevelEndBox" && !LevelEnd)
        {
            LevelEnd = true;
            //UiManage.instance.ManagePanelAppearances();
        }
        if (other.tag == "hubSensor" )
        {
            if (other.GetComponent<HubAriseSensorManager>().automatic)
            {
                other.GetComponent<HubAriseSensorManager>().DecideHubId(transform);
                //ItemHubCentral.instance.AriseHub(i);
            }
            
        }
        if (other.tag == "checkout")
        {

            ItemHubCentral.instance.AriseHubCheckout();
        }

    }


    void CalculateInitials()
    {
        steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        directDistance = steerVector.magnitude;

        //stepsNeeded = (int)(directDistance / AdvanceSpeed);

        //steer = steerVector.x * RotSpeed *(1 - directDistance* zFac* Mathf.Abs( steerVector.x ));
        //steer = (steerVector.x / ((directDistance * zFac) * (directDistance * zFac))) * RotSpeed;

        initialYAngle =Mathf.Rad2Deg * Mathf.Atan2((steerVector.x + 0.001f) , steerVector.z);
        //
        cosA = Mathf.Abs(Mathf.Cos ( Mathf.Deg2Rad * (Mathf.Abs(90 - initialYAngle))));
        float radius = (steerVector.magnitude) / (2 * cosA);
        targetPathLength = radius * Mathf.Deg2Rad *(Mathf.Abs(initialYAngle * 2));
        //
        stepsNeeded =(int)Mathf.Abs(targetPathLength / AdvanceSpeed);
        steer  = (initialYAngle* 2) / stepsNeeded;
        //RotSpeed = initialYAngle / stepsNeeded;
        //steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        //directDistance = steerVector.magnitude;


    }

    public void LeaveDecisionPoint()
    {
        AtDecisionPoint = false;
    }
}

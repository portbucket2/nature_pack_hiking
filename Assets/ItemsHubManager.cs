﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsHubManager : MonoBehaviour
{
    public Transform[] itemSpawnPoints;
    public int maxItems;
    public int gotItems;
    public GameObject item;

    public int itemIndexLocal;

    
    public GameObject[] itemsDisplayed;
    public int OwnHubId;
    public GameObject deer;
    //public GameObject deerOn;

    //public bool underGrounded;

    // Start is called before the first frame update
    void Start()
    {
        DisableDeer();
        if (maxItems > ItemHubCentral.instance.itemIndexLatest)
        {
            ItemHubCentral.instance.itemIndexLatest = maxItems-1;
        }
        itemsDisplayed = new GameObject[maxItems];
        ShuffleSpawnItems();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.U))
        {
            //UpadateItemDispaly();
        }
    }
    
    void ShuffleSpawnItems()
    {
        for (int i = 0; i < itemSpawnPoints.Length; i++)
        {
            if(gotItems != maxItems)
            {
                if ((maxItems - gotItems) < (itemSpawnPoints.Length - i))
                {
                    int r = Random.Range(0, 2);
                    if (r == 1)
                    {
                        itemsDisplayed[gotItems] =  Instantiate(item, itemSpawnPoints[i]);

                        itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().Index = itemIndexLocal;
                        itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().parentHub = this ;
                        gotItems += 1;
                        itemIndexLocal += 1;
                        
                    }
                }
                else
                {
                    itemsDisplayed[gotItems] = Instantiate(item, itemSpawnPoints[i]);

                    itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().Index = itemIndexLocal;
                    itemsDisplayed[gotItems].GetComponent<CollectibleBehavior>().parentHub = this;
                    gotItems += 1;
                    itemIndexLocal += 1;
                }


            }
            
            
        }
    }


    public void UpadateItemDispaly(bool reAppear)
    {
        for (int i = 0; i < itemsDisplayed.Length; i++)
        {
            for (int j = 0; j < ItemHubCentral.instance.CollectedItemList.Count; j++)
            {
                if(itemsDisplayed[i].GetComponent<CollectibleBehavior>().Index == ItemHubCentral.instance.CollectedItemList[j])
                {
                    itemsDisplayed[i].GetComponent<CollectibleBehavior>().Index = ItemHubCentral.instance.itemIndexLatest;
                    
                    
                }
            }
            if (reAppear )
            {
                itemsDisplayed[i].GetComponent<CollectibleBehavior>().EnableOwn();
            }
            //if (reAppear && ItemHubCentral.instance.itemIndexLatest <= ItemHubCentral.instance.maxItemIndex)
            //{
            //    itemsDisplayed[i].GetComponent<CollectibleBehavior>().EnableOwn();
            //}
        }
    }

    public void SinkItemHubPos()
    {
        transform.position = new Vector3(transform.position.x, -10, transform.position.z);
    }

    public void UpliftItemHubPos()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }

    public void EnableDeer()
    {
        deer.SetActive(true);
        Debug.Log("Deer");
        GameManageMent.instance.GetDeer(deer.transform);
        UiManage.instance.CapturePanelAppear();
        GameManageMent.instance.GotDeer = true;
    }

    public void DisableDeer()
    {
        deer.SetActive(false);
    }
}

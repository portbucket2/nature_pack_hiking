﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManageMent : MonoBehaviour
{
    public static GameManageMent instance;
    public int levelIndex;

    public int itemsCollected;
    public int maxItems;
    public int stars;

    public bool GotDeer;
    public bool LevelEnd;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetDeer(Transform t)
    {
        PlayerMotor.instance. deerTarget = t;
        GotDeer = true;
    }

    public void LeaveDeer()
    {
        GotDeer = false;
        UiManage.instance.CloseAllPanels();
        ItemHubCentral.instance.SinkAllHubs();
    }

    public void TakeSnapShot()
    {
        SnapTaker.instance.TakeSnap();
        LevelEnd = true;
        UiManage.instance.LevelEndPanelAppear();
    }
}

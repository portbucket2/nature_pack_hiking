﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHubCentral : MonoBehaviour
{
    public static ItemHubCentral instance;
    public int itemIndexLatest;
    public int maxItemIndex;
    public List<int> CollectedItemList;
    public ItemsHubManager[] itemHubs;

    public delegate void MyDel();
    public MyDel myDel;

    public int AriseHubIdOne;
    public int AriseHubIdTwo;
    public int AriseHubCount;

    public bool GotAll;
    public bool skipOne;
    public int RoadsTravelled;


    // Start is called before the first frame update
    private void Awake()
    {
        AssignOwnIdsToHubs();
        instance = this;
    }
    void Start()
    {
        SinkAllHubs();
        AriseHub(0,0,1);
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCollectedItems(int i)
    {
        bool ignore = false;
        for (int j = 0; j < CollectedItemList.Count; j++)
        {
            if(i == CollectedItemList[j])
            {
                ignore = true;
                break;
            }
        }
        if (!ignore)
            CollectedItemList.Add(i);
        if(CollectedItemList.Count > 5 && !GotAll)
        {
            GameManageMent.instance.stars = 1;
            if (CollectedItemList.Count > 7)
            {
                GameManageMent.instance.stars =2;
                if (CollectedItemList.Count > 9)
                {
                    GameManageMent.instance.stars = 3;
                    GotAll = true;
                    SinkAllHubs();
                }
            }

        }
    }

    public void IncreaseLatestIndex()
    {
        itemIndexLatest += 1;
    }

    public void UpdateAllItemDisplay(ItemsHubManager ownHub)
    {
        for (int i = 0; i < itemHubs.Length; i++)
        {
            if(itemHubs[i] == ownHub)
            {
                itemHubs[i].UpadateItemDispaly(false);
            }
            else
            {
                itemHubs[i].UpadateItemDispaly(true);
            }
            
        }
    }


    public void AriseHub(int id, int idd, int size)
    {
        if (!GotAll)
        {
            AriseHubIdOne = id;
            AriseHubIdTwo = idd;
            AriseHubCount = size;
        }
        else
        {
            AriseHubIdOne = id;
            AriseHubIdTwo = idd;
            AriseHubCount = size;
        }

        RoadsTravelled += 1;
        if(RoadsTravelled == 5)
        {
            skipOne = true;
        }
        
    }
    public void AriseHubCheckout()
    {
        if (!GotAll && !skipOne)
        {
            for (int i = 0; i < itemHubs.Length; i++)
            {
                if (AriseHubCount == 2)
                {
                    if (i == AriseHubIdOne || i == AriseHubIdTwo)
                    {
                        itemHubs[i].DisableDeer();
                        itemHubs[i].UpliftItemHubPos();
                        
                    }
                    else
                    {
                        itemHubs[i].DisableDeer();
                        itemHubs[i].SinkItemHubPos();
                    }
                }
                else if (AriseHubCount == 1)
                {
                    if (i == AriseHubIdOne)
                    {
                        itemHubs[i].DisableDeer();
                        itemHubs[i].UpliftItemHubPos();
                        
                    }
                    else
                    {
                        itemHubs[i].DisableDeer();
                        itemHubs[i].SinkItemHubPos();
                    }
                }

            }
   
        }
        if (skipOne)
        {
            skipOne = false;
            //SinkAllHubs();
            for (int i = 0; i < itemHubs.Length; i++)
            {
                if (AriseHubCount == 2)
                {
                    if (i == AriseHubIdOne || i == AriseHubIdTwo)
                    {
                        itemHubs[i].EnableDeer();
                        //itemHubs[i].UpliftItemHubPos();
                        
                    }
                    else
                    {
                        itemHubs[i].SinkItemHubPos();
                    }
                }
                else if (AriseHubCount == 1)
                {
                    if (i == AriseHubIdOne)
                    {
                        itemHubs[i].EnableDeer();
                        //itemHubs[i].UpliftItemHubPos();
                        
                    }
                    else
                    {
                        itemHubs[i].SinkItemHubPos();
                    }
                }

            }

        }
        else if (RoadsTravelled > 5 && !skipOne)
        {
            skipOne = true;
        }

    }
    public void SinkAllHubs()
    {
        for (int i = 0; i < itemHubs.Length; i++)
        {
            itemHubs[i].SinkItemHubPos();
            itemHubs[i].DisableDeer();

        }
    }

    void AssignOwnIdsToHubs()
    {
        for (int i = 0; i < itemHubs.Length; i++)
        {
            itemHubs[i].OwnHubId = i;
        }
    }

    
}
